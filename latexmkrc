$pdf_mode = 1; 


# Generate /include/revision.tex with git branch and short revision number

open (my $file, '>', 'include/revision.tex');
print $file '\def\ManualRevision{', `git rev-parse --abbrev-ref HEAD `, `git rev-parse --short HEAD `, '}';
print $file '\def\ManualRevisionCounter{', `git rev-list --count master `, '}';
# print $file '\def\ManualRevision{{\href{https://gitlab.camk.edu.pl/araucaria/OCA/oca-manuals/commit/', `git rev-parse HEAD`, '}{', `git rev-parse --abbrev-ref HEAD `, `git rev-parse --short HEAD `, '}}}';
close $file ;


# $pdflatex = 'pdflatex -mltex -output-directory=build -interaction=nonstopmode -shell-escape -aux-directory=build';
$pdflatex = 'pdflatex -interaction=nonstopmode -shell-escape';
$aux_dir = 'build';
$tmpdir  = 'build';
$out_dir = 'build';
